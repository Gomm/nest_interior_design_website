======   HOW TO RUN IT   ======

Clone the repo first, then you'll need 2 terminals

In the first one :

1) cd nest_interior_design_website
2) cd client
3) npm install
4) npm start

In the other :

1) cd nest_interior_design_website
2) cd server
3) npm install
4) nodemon index.js

