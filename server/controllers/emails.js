const nodemailer = require('nodemailer')
const send_email_body = require('../utils/send_email_body')
const transport = nodemailer.createTransport({
	service: 'Gmail',
	auth: {
		user: process.env.NODEMAILER_EMAIL,
		pass: process.env.NODEMAILER_PASSWORD
	}
});  // this is the provider above

const send_email = async (req,res) => {
	// console.log('req.body ======>',req.body)
	// keep the email of the client below, so that you can save it somewhere??
	  const { name , surname, tel, email, message } = req.body //all info needed from contact form ie all fields
	//   const default_subject = 'This is a default subject'
	  const mailOptions = {
		    to: process.env.NODEMAILER_EMAIL, 
		    subject: "New message from " + name + " " + surname,
			html: send_email_body(message,tel,email) // look in utils for this....
	   }
      try{
           const response = await transport.sendMail(mailOptions)
           console.log('=========================================> Email sent !!')
           return await send_user_email(res,req.body)
      }
      catch( err ){
           return res.json({ok:false,message:err})
      }
}

const send_user_email = async (res,data) => {
	//console.log('data ======>',data)
    const mailOptions = {
          to: data.email, 
          subject: "Submission received",
          html: `<p><pre> 
		             Your message: ${data.message} has been received by Nest Interior Design!
	               </pre></p>`
     }
    try{
         const response = await transport.sendMail(mailOptions)
         console.log('=========================================> Email 2 sent !!')
         return res.send({ok: true})
    }
    catch( err ){
         console.log('=====>',err)
         //return res.json({ok:false,message:err})
    } 
}

module.exports = { send_email }