const Orders = require('../models/orders.models.js')
const Users = require('../models/users.models.js')
const jwt        = require('jsonwebtoken');
const jwt_secret = process.env.JWT_SECRET
const { send_emails } = require('../utils/send_order_emails')


const add = async (req,res) => {
    const token = req.headers.authorization;
    const { tel, address, finalCart, message } = req.body
    //console.log('req.body',send_emails)
    jwt.verify(token, jwt_secret, async (err,user) => {
        if(err)return res.send({ok: false})
            console.log(user)
            try{
                await Users.updateOne({_id:user._id},{tel,address})
               const newOrder = await Orders.create({userId:user._id,cart:finalCart})
               console.log(newOrder)
               await send_emails(res,user,finalCart,newOrder._id,message)
            }catch(error){
                console.log(error)
                res.send({ok: false})
            }
        });  
}

const remove = async (req,res) => {
    console.log ('remove order', req.body)
    const { _id } = req.body
    try{
        const removed = await Orders.deleteOne({_id:_id})
        console.log(removed) 
        res.send({ok: true, removed})
    }catch(err){
        res.send({ok: false})
    }
}

const findOrdersByUser = async (req,res) => {
    const token = req.headers.authorization;
    jwt.verify(token, jwt_secret, async (err,user) => {
        if(err)return res.send({ok: false})
        try{
            const foundOrder = await Orders.find({userId: user._id})
            console.log('found order',foundOrder) 
            res.send({ok: true, foundOrder})
        }catch(error){
            console.log(error)
            res.send({ok: false})
        }
    });
}

//===============================================================================
const fetchUsers = async(orders) => {
    const usersIds = []
    orders.forEach( order => usersIds.push(order.userId))
    const users = await Users.find( { _id : { $in :  usersIds } } );
    return users
}
//===============================================================================

const findAllOrders = async (req,res) => {
    const token = req.headers.authorization;
    jwt.verify(token, jwt_secret, async (err,user) => {
        if(err && !user.admin)return res.send({ok: false})
        try{
            const orders = await Orders.find({}) 
            const users = await fetchUsers(orders)
            // console.log('orders ==>',orders)
            res.send({ok: true, orders, users})
        }catch(error){
            console.log(error)
            res.send({ok: false})
        }
    });
}

const updateOne = async (req,res) => {
    const { date,  progress, payment, notes, cart, userId, _id }  = req.body 
    console.log(req.body)   
    try{
    const updated = await Orders.updateOne({_id:_id},{ date: date, progress: progress, payment: payment, notes: notes, cart:cart, userId: userId})
    console.log('updated ==>',updated)
    //const orders = await Orders.find({})
    //console.log('orders ==>',orders)
        res.send({ok: true, updated})
      
    } catch (err){
        //console.log('err ==>',err)
        res.send ({ok: false})
    }
}




module.exports = {
    add,
    remove,
    findOrdersByUser,
    findAllOrders,
    updateOne,
   
   
}

// const findOrdersByUser = async (req,res) => {
//     const token = req.headers.authorization;
//     const foundOrder = await Orders.find({_id: _id}) 
//     res.send({ok: true, foundOrder})
//     jwt.verify(token, jwt_secret, async (err,user) => {
//         if(err)return res.send({ok: false})
//             try{

//             }catch(error){
//                 console.log(error)
//                 res.send({ok: false})
//             }
//     });
// }