const Categories = require('../models/categories.models')
const Products = require('../models/products.models.js') 
const mongoose = require('mongoose')

const create = async (req,res) => {
    const { category } = req.body
    try{
        const created = await Categories.create({ category })
        res.send({ok: true,created})
    }catch(err){
        res.send({ok: false,err})
    }
}


const remove = async (req,res) => {
    const { _id } = req.body
    try{
        const removed = await Categories.deleteOne({_id:_id}) // you could just omit the second part('ie the value') id the are the same eg just {_id}
        const removeProducts = await Products.deleteMany({categoryId: _id})
        const products = await Products.find({})
        console.log('====>',removeProducts)
        res.send({ok: true, removed, products })
    }catch(err){
        console.log(err)
        res.send({ok: false})
    }
}

const updateOne = async (req,res) => {
    const { _id, category } = req.body    
    try{
        const updated = await Categories.updateOne({_id:_id},{category: category})
        console.log(updated)
        const categories = await Categories.find({}) 
        res.send({ok: true, updated, categories})
    } catch (err){
        res.send ({ok: false})
    }
}

const findAll = async (req,res) => {
    try{
        const categories = await Categories.find({}) 
        res.send({ok: true, categories})
    }catch(err){
        res.send({ok: false})
    }
}

module.exports = {
    create,
    remove,
    updateOne,
    findAll
}