const Products = require('../models/products.models.js')
const Categories = require ('../models/categories.models.js')

const create = async (req,res) => {
    const { product, image, price, color, description, categoryId } = req.body
    console.log(req.body)
    try{
        const created = await Products.create({ product, image, price, color, description, categoryId })
        res.send({ok: true,created})
    }catch(err){
        res.send({ok: false})
    }
}

const remove = async (req,res) => {
    console.log ('remove product', req.body)
    const { _id } = req.body
    try{
        const removed = await Products.deleteOne({_id:_id}) // you could just omit the second part('ie the value') id the are the same eg just {_id}
        res.send({ok: true, removed})
    }catch(err){
        res.send({ok: false})
    }
}

const findOne = async (req,res) => {
    const { _id } = req.body
    try{
        const foundProduct = await Products.findOne({_id: _id}) // .find will give you an array of objects and .findOne will give you back just one object
        res.send({ok: true, foundProduct})
    }catch(err){
        res.send({ok: false})
    }
}

const findAll = async (req,res) => {
    try{
        const products = await Products.find({}) // .find will give you an array of objects, so we can just pass an empty object
        res.send({ok: true, products})
    }catch(err){
        res.send({ok: false})
    }
}

const updateOne = async (req,res) => {
    const { _id, product, image, price, color, description } = req.body    
    try{
    const updated = await Products.updateOne({_id:_id},{product: product, image: image, price: price,color: color,description: description})
    const products = await Products.find({})
        res.send({ok: true, updated, products})
      
    } catch (err){
        res.send ({ok: false})
    }
}

const findProducts = async (req,res) => {
    const { categoryId } = req.body
    try{
        const products = await Products.find({categoryId: categoryId}) // .find will give you an array of objects, so we can just pass an empty object
        res.send({ok: true, products})
    }catch(err){
        res.send({ok: false})
    }
}

// ================================================================>

const getCategoriesAndProducts = async (req,res) => {
    try {
        const products = await Products.find({})
        const categories = await Categories.find ({})
        res.send({ok: true, products, categories})
    } catch(err){
        res.send({ok: false})
    }
}

//=================== GET CART ===================================

const findCart = async (req,res) => {
    try {
        const { cart } = req.body
        const tempArray = []
        cart.forEach ((item)=>{
            tempArray.push(item._id)
        })
        const products = await Products.find( { _id : { $in : tempArray } } );
        res.send({ok: true, products})
    } catch(err){
        res.send({ok: false})
    }
}



module.exports = {
    create,
    remove,
    findOne,
    findAll,
    updateOne,
    findProducts,

    getCategoriesAndProducts,
    findCart
}