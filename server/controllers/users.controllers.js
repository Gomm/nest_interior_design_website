const User       = require('../models/users.models'); 
const bcrypt     = require('bcrypt');
const jwt        = require('jsonwebtoken');
const validator  = require('validator');
const jwt_secret = process.env.JWT_SECRET


const register = async (req,res) => {
    const { name, surname, email , password , password2 } = req.body;
	if(!name || !surname || !email || !password || !password2 ) return res.json({ok:false,message:'All field are required'});
    if( password !== password2 ) return res.json({ok:false,message:'Passwords must match'});
    if (password.length < 8) return res.json({ok:false,message:'Password must be at least 8 characters'});
    if( !validator.isEmail(email) ) return res.json({ok:false,message:'Please provide a valid email'})
    try{
        const user = await User.findOne({ email })
    	if( user ) return res.json({ok:false,message:'Email already in use'});
        console.log('==========',user)
    	const hash = await bcrypt.hash(password,10)
        console.log('hash =' , hash)
        const newUser = {
            name,
            surname,
        	email,
            password : hash,
            //admin:true
        }
        await User.create(newUser)
        res.json({ok:true,message:'Successful registration'})
    }catch( error ){
        res.json({ok:false,error})
    }
}

const login = async (req,res) => {
    const { email , password } = req.body;
    if( !email || !password ) res.json({ok:false,message:'All field are required'});
    if( !validator.isEmail(email) ) return res.json({ok:false,message:'Please provide a valid email'});
	try{
    	const user = await User.findOne({ email });
    	if( !user ) return res.json({ok:false,message:'Please provide a valid email'});
        const match = await bcrypt.compare(password,user.password);
        if(match) {
           const token = jwt.sign(user.toJSON(), jwt_secret ,{ expiresIn:100080 });//{expiresIn:'365d'}
           res.json({ok:true,message:'Welcome back',token,email,admin:user.admin}) 
        }else return res.json({ok:false,message:'Invalid password'})
    }catch( error ){
    	 res.json({ok:false,error})
    }
}

const verify_token = (req,res) => {
  console.log(req.body.token)
       const { token } = req.body;
       jwt.verify(token, jwt_secret, (err,succ) => {
             err ? res.json({ok:false,message:'Something went wrong'}) : res.json({ok:true,succ})
       });      
}


//==================== FOR THE USERS/ CLIENTS PAGE =============================================


// const create = async (req,res) => {
//     const { name , surname , admin  } = req.body
//     console.log(req.body)
//     try{
//         const created = await User.create({  })
//         res.send({ok: true,created})
//     }catch(err){
//         res.send({ok: false})
//     }
// }

const remove = async (req,res) => {
    console.log ('remove user', req.body)
    const { _id } = req.body
    try{
        const removed = await User.deleteOne({_id:_id}) 
        res.send({ok: true, removed})
    }catch(err){
        res.send({ok: false})
    }
}

const findOne = async (req,res) => {
    const { _id } = req.body
    try{
        const foundUser = await User.findOne({_id: _id})
        res.send({ok: true, foundUser})
    }catch(err){
        res.send({ok: false})
    }
}

const findAll = async (req,res) => {
    try{
        console.log(`==============> users`)
        const users = await User.find({}) 
        res.send({ok: true, users})
    }catch(err){
        res.send({ok: false})
    }
}

const updateOne = async (req,res) => {
    const { name , surname , tel , email , address , admin , _id } = req.body    
    try{
    const updated = await User.updateOne({_id:_id},{name: name, surname: surname, tel: tel, email: email, address: address, admin: admin})
    const users = await Users.find({})
        res.send({ok: true, updated, users})
      
    } catch (err){
        res.send ({ok: false})
    }
}







module.exports = { 
    register ,
    login ,
    verify_token ,

    // create ,
    remove ,
    findOne ,
    findAll ,
    updateOne
}
