const router = require('express').Router();
const controller = require('../controllers/users.controllers')
//const express = require('express')

router.post('/register',controller.register);
router.post('/login',controller.login);
router.post('/verify_token',controller.verify_token);


//router.post('/create', controller.create)
router.post('/remove', controller.remove)
router.post('/find_one', controller.findOne)
router.get('/find_all', controller.findAll)
router.put('/update_one', controller.updateOne)



module.exports = router;