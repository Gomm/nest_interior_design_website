const express = require('express')
const router  = express.Router() 
const controller = require('../controllers/orders.controllers.js')


router.post('/add', controller.add)
router.post('/remove', controller.remove)
router.post('/find_one', controller.findOrdersByUser)
router.get('/find_all', controller.findAllOrders)
router.put('/update_one', controller.updateOne)



module.exports = router 