const express = require('express')
const router  = express.Router() 
const controller = require('../controllers/categories.controllers.js')


router.post('/create', controller.create)
router.post('/remove', controller.remove)
router.put('/update_one', controller.updateOne)
router.get('/find_all', controller.findAll)

module.exports = router 