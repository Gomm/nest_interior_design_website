const express = require('express')
const router  = express.Router() 
const controller = require('../controllers/products.controllers.js')


router.post('/create', controller.create)
router.post('/remove', controller.remove)
router.post('/find_one', controller.findOne)
router.get('/find_all', controller.findAll)
router.put('/update_one', controller.updateOne)
router.get('/find_products', controller.findProducts)
router.get('/getCategoriesAndProducts', controller.getCategoriesAndProducts)

router.post('/find_cart', controller.findCart)

module.exports = router 