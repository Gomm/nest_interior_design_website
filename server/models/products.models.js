const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const ProductsSchema = new Schema({
    product : {
        type    : String,
        unique  : true,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    price : {
        type    : String,
        required: true
    },
    color : {
        type    : String,
        required: true
    },
    description : {
        type    : String,
        required: true
    },
    categoryId : {
        type    : String,
        required: true
    }
})

module.exports = mongoose.model('products', ProductsSchema)