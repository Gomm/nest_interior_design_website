const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    name: {type:String, required:true},
    surname: {type:String, required:true},
    email:{ type:String, unique:true, required:true },
    tel: {type: String, required: false, default: ''},
    address: {type: String, required: false, default: ''},
    password:{ type:String, required:true },
    admin:{type:Boolean, default: false}
});
module.exports = mongoose.model('users', userSchema);