const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const CategoriesSchema = new Schema({
    category : {
        type    : String,
        unique  : true,
        required: true
    }
})

module.exports = mongoose.model('categories', CategoriesSchema)