const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const OrdersSchema = new Schema({
    date: {
        type: Date,
        // `Date.now()` returns the current unix timestamp as a number
        default: Date.now
    },
    
    progress: {
        type    : String,
        required: false,
        default : 'pending'
    },
    payment: {
        type    : String,
        required: false,
        default : 'unpaid'
    },

    notes: {
        type    : String,
        required:false,
        default : ''
    },
   
    cart : {
        type    : Array,
        required: true
    },

    userId: {
        type    : String,
        required: true
    }
})

module.exports = mongoose.model('orders', OrdersSchema)