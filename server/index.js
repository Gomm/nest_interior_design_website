const express  = require('express');
const path = require('path');
const app = express()

const port = process.env.port || 8080;
require('dotenv').config()
// =============== BODY PARSER SETTINGS =====================
const bodyParser= require('body-parser');
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// =============== DATABASE CONNECTION =====================
const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
async function connecting(){
    try {
        // used to be .....'mongodb://127.0.0.1/auth'
        await mongoose.connect(`${process.env.ATLAS_URL_NEST}`, { useUnifiedTopology: true , useNewUrlParser: true })
        console.log('Connected to the DB')
    } catch ( error ) {
        console.log('ERROR: Seems like your DB is not running, please start it up !!!');
    }
}
connecting()    
//================ CORS ================================
const cors = require('cors');
app.use(cors());
// =============== ROUTES ==============================
app.use('/users', require('./routes/users.routes'));
app.use('/categories', require('./routes/categories.routes'))
app.use('/products', require('./routes/products.routes'))
app.use('/emails', require('./routes/emails.js'))
app.use ('/orders', require('./routes/orders.routes'));

//================ STATICS ================================
app.use(express.static(__dirname));
app.use(express.static(path.join(__dirname, '../client/build')));

app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname, '../client/build', 'index.html'));
});

// =============== START SERVER =====================
app.listen(port, () => 
    console.log(`server listening on port ${port}`
));
