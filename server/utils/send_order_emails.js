const nodemailer = require('nodemailer')
// Located in orders.controllers.js in the add function

const orderNumberText = 'Order Number:'

const transport = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: process.env.NODEMAILER_EMAIL,
            pass: process.env.NODEMAILER_PASSWORD
        }
});

const send_emails = async (res,user,finalCart,orderNumber,message) => {
	  const mailOptions = {
		    to: process.env.NODEMAILER_EMAIL, 
            subject: "New order from " + user.name,
              html: `<p><pre>${orderNumberText} ${orderNumber}</pre><p>${message}</p></p>`
              //???also add <p>${finalCart}</p>
	   }
    //    console.log('=====>',orderNumber)
      try{
           const response = await transport.sendMail(mailOptions)
           console.log('=========================================> Email sent !!')
            return await send_second_email(res,user,orderNumber)
      }
      catch( err ){
           console.log('=====>',err)
           //return res.json({ok:false,message:err})
      } 
}

const send_second_email = async (res,user,orderNumber) => {
    const mailOptions = {
          to: user.email, 
          subject: "Order received by Claire",
          html: `<p><pre>
                     If you have any questions regarding your order feel free to contact Claire via email and use
                     your Order Number: ${orderNumber} as a reference.
                 </pre></p>`
     }
    try{
        console.log('=====>',orderNumber)
         const response = await transport.sendMail(mailOptions)
         console.log('=========================================> Email sent !!')
         return res.send({ok: true})
    }
    catch( err ){
         console.log('=====>',err)
         //return res.json({ok:false,message:err})
    } 
}

module.exports = {
    send_emails,
}