import React , { useState } from 'react'
import { Button, Modal } from 'semantic-ui-react'

//import Axios from 'axios' 

const Create = (props) => {
    const [data,setData] = useState({})
    const [category, setCategory] = useState('')
    const [newCategory, setNewCategory] = useState ('')


    const categorySelect = (e) =>{
        if(e.target.value === 'All'){
            
        }
        //debugger
        const filtered = props.products.filter( item => item.categoryId === e.target.value)
        setCategory(e.target.value)
        
        //debugger
    }
    

    const handleSubmit=  e =>{
        e.preventDefault()
     
        alert(`New Category ${newCategory} has been succesfully added!`)
        props.createCategory(newCategory)
        setNewCategory('')

     }

     const [alertOpen, setAlertOpen] = useState(false)
     const alertShow = () => setAlertOpen (true)
     const alertClose = () => setAlertOpen(false)


     const handleCreateNewProduct = e => {
         e.preventDefault()
         debugger
         if(!data.product || !data.image || !data.price || !data.color || !data.description || !category) {
            return alertShow(true)
         } 
         props.createNewProduct(data,category)
         setData({
             product: '',
             image: '',
             price: '',
             color: '',
             description: '',
         })
         alert(`New product ${data.product} has been successfully created!`)
        
         //debugger
     }


    return <div>

        <Modal size={'small'} open={alertOpen} onClose={alertClose}>
            <Modal.Header>Error</Modal.Header>
            <Modal.Content>
                <p>Make sure you have selected a valid category and all fields are filled in to create a new Product!</p>
            </Modal.Content>
            <Modal.Actions>
                <Button onClick={()=> alertClose()} negative>Got it</Button>
            </Modal.Actions>
        </Modal>



        <form className='form_container' onSubmit = {handleSubmit}>
            <h1>Create New Category</h1>
            <label>Category:</label>
            <input  value={newCategory} onChange={e=>setNewCategory(e.target.value)}/>
            <button>Add</button>
            
        
        </form>

        <div className='form_container'>
            <h1>Create New Product</h1>

            <label>Select Category: </label>
            <select value={category} onChange={categorySelect}>
            <option value={'All'}>All</option>
                {
                     props.categories.map((ele,i)=>{
                         return  <option key={i} value={ele._id}>{ele.category}</option>
                     })  
                }
            </select>
           

            <label>Product: </label>
            <input onChange={(e)=>setData({...data,[e.target.name]:e.target.value})}
                   value={data.product} 
                   name='product' 
            />

            <label>Image: </label>
            <input placeholder='  insert image url here'
                   onChange={(e)=>setData({...data,[e.target.name]:e.target.value})}
                   value={data.image} 
                   name='image' 
            />
            
            <br></br>
            {/* <div>
                <h4>Image preview: </h4>
                <div className='preview_image'>{data.image}</div>
            </div> */}

            <label>Price: </label>
            <input onChange={(e)=>setData({...data,[e.target.name]:e.target.value})}
                   value={data.price} 
                   name='price' 
            />

            <label>Color: </label>
            <input onChange={(e)=>setData({...data,[e.target.name]:e.target.value})}
                   value={data.color} 
                   name='color' 
            />

            <label>Description: </label>
            <textarea onChange={(e)=>setData({...data,[e.target.name]:e.target.value})}
                      value={data.description} 
                      name='description' 
            />

            <button onClick= {handleCreateNewProduct}>Save</button>


        </div>
    </div>
}

export default Create