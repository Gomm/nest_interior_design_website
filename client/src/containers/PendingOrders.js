import React, {useState, useEffect} from 'react'
import axios from 'axios'
import { URL } from '../utils/url_config' 


const PendingOrders = (props) => {
    const [orders, setOrders] = useState ([])
     
    useEffect( () => {
        fetchOrders()
    },[])

    const fetchOrders = async () => {
        try{
            axios.defaults.headers.common['Authorization'] = JSON.parse(localStorage.getItem('token'))
            const response = await axios.post(`${URL}/orders/find_one`)
            //debugger
			setOrders ([...response.data.foundOrder])
			
		}catch(err){
			console.log (err)
		}
    }

    const totalPrice = (cart) => {
		
        let total = 0
        cart.forEach( ele => total += ele.price*ele.quantity)
        return total
    }


////////////////////////////////////////////////////////////////////////
////////////////////////// PAGE STARTS /////////////////////////////////
    const colors = {
        'pending': 'red',
        'in progress': 'purple',
        'done': 'green',
        'unpaid': 'red',
        'deposit paid': 'purple',
        'paid': 'green',
    }

    

    return <div>
        

        <div >

            
                {
					orders.map ((obj,i)=>{
						return ( 
							<div className='form_container'>
                            <h3>Order number: {obj._id}</h3>

                            <button style={styles[colors[obj.progress]]}>Progress Status:  {obj.progress}</button>
                            
                            <button style={styles[colors[obj.payment]]}>Payment Status:  {obj.payment}</button>

                            <br></br>
                            <p>Notes: {obj.notes}</p>
                            
                               {
                                obj.cart.map((order)=>{
                                    return (
                                        <div>
                                            <h2>{order.product}</h2>
                                            <h4>Unit Price: K{order.price}</h4>
									<div>
									    <img className='img' src = {order.image}/>
									</div>
								     <h4>Qty: {order.quantity}</h4>
								<h4>Product Total: K{order.quantity*order.price}</h4>
                                
                                <br></br>
								
                                            
                                        </div>
                                    )
                                })
                                }
                                <h2>Order Total: K{totalPrice(obj.cart)}</h2> 
							</div>
						);
					})
				
				}

        </div>


        </div>
}

export default PendingOrders

const styles = {
    red: {
      backgroundColor: "red"
    },
    purple: {
        backgroundColor: "purple"
    },
    green: {
        backgroundColor: "green"
    }
}