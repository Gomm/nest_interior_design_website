import React, {useState, useEffect} from 'react';
import Axios from 'axios'; 

import { Button, Header, Icon, Modal } from 'semantic-ui-react'

const Shop = (props) => {
    const [products, setProducts] = useState([])
    const [category, setCategory] = useState('All')
    const [open, setOpen] = useState(false)
      
    useEffect(()=>{
        setProducts([...props.products])
    },[props.products])
   
    const categorySelect = (e) =>{
        if(e.target.value === 'All'){
            return setProducts([...props.products])
        }
        //debugger
        const filtered = props.products.filter( item => item.categoryId === e.target.value)
        setCategory(e.target.value)
        setProducts([...filtered])
        //debugger
    }

    //=================== MY ORDER ==========================

    const handleAddToOrder = (product) => {
        //debugger
        if (!props.isLoggedIn)return setOpen(true)
        const cart = JSON.parse(localStorage.getItem('cart'))
        const index = cart.findIndex(item => item._id === product._id) 
            if (index < 0){
                cart.push({_id:product._id,quantity:1})
                localStorage.setItem('cart', JSON.stringify(cart))
                alert ('Product has been added to your order')
                
            } else {
                alert ('This product is already in your cart! If you would like to increase quantities, you can do so in your Cart')
            }
        //debugger
    }

    const goToLogin = () => {
        setOpen(false)
        props.history.push('/login');
    }
		
   

    return <div>

        <div className='form_container'>
            {/**********************************************************************************/}

            <Modal           
                open={open}
                onClose={()=>setOpen(false)} closeIcon>
                <Header icon='archive' content='Archive Old Messages' />
                <Modal.Content>
                <p>
                    You need to be Registered and Logged in to place an order! Would you like to proceed to Login?
                </p>
                </Modal.Content>
                <Modal.Actions>
                <Button color='red' onClick={()=>setOpen(false)}>
                    <Icon name='remove' /> No
                </Button>
                <Button color='green' onClick={goToLogin}>
                    <Icon name='checkmark' /> Yes
                </Button>
                </Modal.Actions>
            </Modal>

            {/**********************************************************************************/}
            <label>Select Category:</label>
                <select value={category} onChange={categorySelect}>
                <option value={'All'}>All</option>
                    {
                        props.categories.map((ele,i)=>{
                            return  <option key={i} value={ele._id}>{ele.category}</option>
                        })  
                    }
                </select>
        </div>

       
            {
                products.map ((obj,i)=>{
                    return ( 
                        <div className='form_container'>
                            <h1>{obj.product}</h1>
                            <h3>K{obj.price}</h3>
                            <div className='image_box'>
                                <img className='img' src  = {obj.image}/> 
                            </div>
                            <p>{obj.color}</p>
                            <p>{obj.description}</p>
                            {
                                !props.isAdmin 
                                ? <button onClick= {()=>handleAddToOrder(obj)}>Add to order</button>
                                : null
                            }
                            
                        </div>
                    );
                })          
            }
    

    </div>

}

export default Shop;