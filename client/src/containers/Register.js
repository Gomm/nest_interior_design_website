import React , { useState, useEffect } from 'react'
import Axios from 'axios' 
import { URL } from '../utils/url_config' 

const Register = (props) => {

	
	const [ form , setValues ] = useState({
		name	 : '',
		surname  : '',
		email    : '',
		password : '',
		password2: ''
	})
	const [ message , setMessage ] = useState({message:'',ok:false})
	useEffect(()=>{
      if(message.ok){
		props.history.push('/login')
	  }
	},[message])

	const handleChange = e => {
       setValues({...form,[e.target.name]:e.target.value})
	}
	const handleSubmit = async (e) => {
		e.preventDefault()
		try{ 
			
			const response =  await Axios.post(`${ URL }/users/register`,{
				name	 : form.name,
				surname  : form.surname,
				email    : form.email,
			    password : form.password,
			    password2: form.password2
	        })
	        setMessage({message:response.data.message,ok:response.data.ok})
			//console.log(response)
			
		}
		catch( error ){
			console.log(error)
		}

	}
	return <form onSubmit={handleSubmit}
	             onChange={handleChange}
	             className='form_container'>
			 <label>Name</label>
		     <input name="name"/>
			 <label>Surname</label>
		     <input name="surname"/>
	         <label>Email</label>
		     <input name="email"/>
		     <label>Password</label>
		     <input type='password' name="password"/>
		     <label>Confirm password</label>
		     <input type='password' name="password2"/>
		     <button>Sign Up</button>
		     <div className='message'><h4>{message.message}</h4></div>
	       </form>
}

export default Register