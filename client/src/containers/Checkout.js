import React, {useState,useEffect} from 'react'
import axios from 'axios'
import { URL } from '../utils/url_config' 

const Checkout  = (props) => {
	const cart = JSON.parse(localStorage.getItem('cart'))
	const [finalCart, setFinalCart] = useState ([])
	const [data, setData] = useState({})
	const [disable, setDisable] = useState(false)

	
    useEffect(()=>{
		fetchCartItems()
	},[])
	
	useEffect(()=>{
		if(disable !== false){
			handlePlaceOrder()
		}
    },[disable])
    
		
	const fetchCartItems = async () => {
		try{
			const response = await axios.post(`${URL}/products/find_cart`,{cart})
			response.data.products.forEach( item => {
				const index   = cart.findIndex( ele => ele._id == item._id )
				item.quantity = cart[index].quantity
				
			})
			setFinalCart ([...response.data.products])
			
            //call server to get products in cart from local storage
		}catch(err){
			console.log (err)
		}
	}

	const totalPrice = () => {
	
		let total = 0
		finalCart.forEach( ele => total += ele.price*ele.quantity)
		return total
	}

	const returnToMyOrder = () => {
		props.history.push('/myOrder');
	}

//=================== HANDLE PLACE ORDER ==================================

	const addOrder = async() => {
		// this route to ${URL} orders/add is where the function of sending the emails occurs
		try{
		   axios.defaults.headers.common['Authorization'] = JSON.parse(localStorage.getItem('token'))
		   const response = await axios.post(`${URL}/orders/add`,{
																 ...data,
																 finalCart		
		   })
		   return response.data.ok 
		}catch(err){
		console.log (err)
		 
		}
	 }
		

	const handlePlaceOrder = async () => {

		if(!data.message || !data.tel || !data.address){
			return alert('All fields are required to place an order')
		}

		const response = await addOrder()
	
		response 
		? props.history.push('/orderPlacedMessage')
		
		: alert('Something wrong happened')
		// remove final cart from local storage 
		localStorage.setItem('cart', JSON.stringify([]))
     
    }

    const handleChange = (e) => {
		setData({...data,[e.target.name] : e.target.value})
		// push tel and address to users db
		// send order to orders db
    }	





	return <div className='myOrder'>

				

				<div className='form_container' >
					<h1>Order Form</h1>
					<p>Please fill in your details</p>
				
					<label>Tel: </label>
					<input name ='tel'
						   onChange={handleChange}/>
				
					<label>Address: </label>
					<input name='address'
						   onChange={handleChange}/>
					
					<label>Message:</label>
					<textarea name = 'message'
					          onChange={handleChange}>Your message here ...</textarea>

					<h2>Order Total: K{totalPrice()}</h2> 
					<button onClick={returnToMyOrder}>Make a change</button>
					<br></br>
					<button disabled={disable === true}
					        onClick={()=>setDisable(true)}>Place Order</button>
					

					<h1>Your final Order</h1>
						{
							finalCart.map ((obj,i)=>{
								return ( 
									<div>
										<h3>Product: {obj.product}</h3>
										<h4>Unit Price: K{obj.price}</h4>
											<div>
												<img src = {obj.image}/>
											</div>
										<h4>Qty: {obj.quantity}</h4>
										<h4>Product Total: K{obj.price*obj.quantity}</h4>
										<br></br>
									</div>
								);
							})
						
						}
							
				 
				 </div>

	       </div>
	   
}

export default Checkout



// Below is another way in which you could push state to a new page if needed..........

// const handlePlaceOrder = () => {
// 	props.history.push({
// 				  pathname:'/orderForm',
// 				  state: {finalCart:finalCart}
// 				})   
// }	

// And this is how you would get it in the file that you want to use the state...............

// useEffect( ()=> {
// 	console.log('props =>',props.location.state.finalCart)
// },[]) 
