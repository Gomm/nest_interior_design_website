import React, {useState, useEffect,} from 'react'

const UpdateProducts = props => {

    const [data,setData] = useState({})
    const [category, setCategory] = useState()

    return <div className='form_container'>
    <h1>{props.obj.product}</h1>
        <label>Product: </label> 
            <input onChange={(e)=>setData({...data,[e.target.name]:e.target.value})}
                   value={data.product} 
                   name='product' 
                   placeholder = {props.obj.product}/> 
            <br></br>
        <label>Price: </label>
            <input onChange={(e)=>setData({...data,[e.target.name]:e.target.value})}
                   value={data.price} 
                   name='price'
                   placeholder={props.obj.price}
            /> 
            <br></br>
        <div className='image_box'>
            <img className='img' src = {props.obj.image}/> 
        </div>
        <label>Image url: </label>
        <input onChange={(e)=>setData({...data,[e.target.name]:e.target.value})}
                   value={data.image} 
                   name='image'
                   placeholder= {props.obj.image}/>

        <label>Description: </label>
        <textarea onChange={(e)=>setData({...data,[e.target.name]:e.target.value})}
                  value={data.description} 
                  name='description' >{props.obj.description}</textarea>

      

        <button onClick= {()=>{setCategory(props.obj.categoryId); props.updateProduct(data,category,props.obj)}}>Save changes</button>
    </div>





}

export default UpdateProducts