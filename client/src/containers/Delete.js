import React, {useState, useEffect} from 'react';
import { Button, Modal } from 'semantic-ui-react'

const Delete = (props) => {
    const [products, setProducts] = useState([])
    const [category, setCategory] = useState('All products')
    const [product, setProduct] = useState('')
  
   
    useEffect(()=>{
        setProducts([...props.products])
    },[props.products])
   
    const categorySelect = (e) =>{
        if(e.target.value === 'All products'){
            return setProducts([...props.products])
        }
        //debugger
        const filtered = props.products.filter( item => item.categoryId === e.target.value)
        setCategory(e.target.value)
        setProducts([...filtered])
        //debugger
    }

    
        const [categoryOpen, setCategoryOpen] = useState (false)
        const [productOpen, setProductOpen] = useState(false)
      
        const categoryShow = () => setCategoryOpen(true)
        const productShow = () => setProductOpen (true)
        const categoryClose = () => setCategoryOpen(false)
        const productClose = () => setProductOpen(false)

    const renderModal = () => {
        return         <Modal size={'small'} open={productOpen} onClose={productClose}>
                            <Modal.Header>Delete Product</Modal.Header>
                            <Modal.Content>
                            <p>Are you sure you want to delete this product?</p>
                            </Modal.Content>
                            <Modal.Actions>
                            <Button onClick={()=> productClose()} negative>No</Button>
                            <Button
                                onClick={()=>{productClose();props.deleteProduct(product)}}
                                positive
                                icon='checkmark'
                                labelPosition='right'
                                content='Yes'
                            />
                            </Modal.Actions>
                        </Modal>
    }


///////////////////////////////////////////////////////////////////////////////////
////////////////////////// PAGE STARTS ///////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

    return <div>

        <Modal size={'small'} open={categoryOpen} onClose={categoryClose}>
          <Modal.Header>Delete Category</Modal.Header>
          <Modal.Content>
            <p>Are you sure you want to delete this Category? There may be products in this category that you may not want to delete?</p>
          </Modal.Content>
          <Modal.Actions>
            <Button onClick={()=> categoryClose()} negative>Go back and check</Button>
            <Button
              onClick={()=>{categoryClose();props.deleteCategory(category)}}
              positive
              icon='checkmark'
              labelPosition='right'
              content='Delete Anyway'
            />
          </Modal.Actions>
        </Modal>

    
        {renderModal()}

        <div className='form_container'>
            <h1>Delete Category</h1>

            <label>Select Category:</label>
                <select value={category} onChange={categorySelect}>
                <option value={'All products'}>All products</option>
                    {
                        props.categories.map((ele,i)=>{
                            return  <option key={i} value={ele._id}>{ele.category}</option>
                        })  
                    }
                </select>


            <button onClick= {()=>categoryShow(category)}>Delete Category</button>
            <div>
                <h5>{props.message}</h5>
            </div>
        
        </div>

        

        {
            products.map ((obj,i)=>{
                return ( 
                    <div className='form_container'>
                        <h1>{obj.product}</h1>
                        <br></br>
                        <h3>K{obj.price}</h3>
                        <br></br>
                        <div className='image_box'>
                            <img src = {obj.image}/> 
                        </div>
                        <p>{obj.color}</p>
                        <p>{obj.description}</p>
                        
                        
                        <button onClick={ ()=>{setProduct(obj._id);productShow()}}>Delete Product</button>
                    </div>


                );
            })
          
        }
    

    </div>
}

export default Delete