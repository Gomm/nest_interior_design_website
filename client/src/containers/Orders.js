import React, {useState, useEffect} from 'react'
import axios from 'axios'
import { URL } from '../utils/url_config' 
import { Button, Modal } from 'semantic-ui-react'
import Axios from 'axios'

const Orders = (props) => {

    const [orders, setOrders] = useState([])
    const [orderList, setOrderList] = useState ([])
    const [id, setId] = useState ()
    const [newNote, setNewNote] = useState('')

    const [orderId, setOrderId] = useState()
    const [deleteOpen, setDeleteOpen] = useState(false)
    const deleteShow = () => setDeleteOpen (true)
    const deleteClose = () => setDeleteOpen(false)


    useEffect(()=>{
        fetchOrders()
    },[])

        
        const totalPrice = (cart) => {

        let total = 0
        cart.forEach( ele => {
                total += ele.price*ele.quantity
        })    
        return total
        }

        const fetchOrders = async () => {
        try{
            axios.defaults.headers.common['Authorization'] = JSON.parse(localStorage.getItem('token'))
			const response = await axios.get(`${URL}/orders/find_all`)
            const { users } = response.data
            const temp = []
            response.data.orders.forEach( order => {
                const index = users.findIndex( user => user._id == order.userId)
                //debugger
                order.usersDetails = {
                        tel: users[index].tel,
                        address: users[index].address,
                        name: users[index].name,
                        surname: users[index].surname,
                        email: users[index].email
                }
                temp.push(order)
            })
            setOrders ([...temp])   
            setOrderList ([...temp])  
		    }catch(err){
			console.log (err)
		    }
        }

//================== DELETE ORDER =========================================

        const deleteOrder = async (orderId) =>{
            //debugger
            try{
                const response = await axios.post(`${URL}/orders/remove`,{_id: orderId})
                //debugger
                const temp = orders
                const index = temp.findIndex( order => order._id === orderId)
                temp.splice(index,1)
                setOrders ([...temp])
                setOrderList ([...temp])
            }catch(err){
                console.log(err)
                debugger
            }
        }

//=================== HANDLE DELETE =======================================




    const handleOrderNumber = (e) => {
        setId(e.target.value)
    }

    const findByOrderNumberClicked = () => {
        //debugger
        orders.map((obj,i)=>{
            console.log(obj._id)
            if (id === obj._id){
               //debugger
               console.log(obj)
                setOrderList([obj])
            } else {
                //return alert (`This order does not exist`)
            }
        })
    }


    const progressSelect = (e) =>{
        if(e.target.value === 'all'){
            setOrderList([...orders])
            return orderList
        }
        const filtered = orders.filter( item => item.progress === e.target.value)
        // debugger
        setOrderList([...filtered])
 
    }


    const paymentSelect = (e) =>{
        if(e.target.value === 'all'){
            let temp = orders
            setOrderList([...temp])
        }
        //debugger
        const filtered = orders.filter( item => item.payment === e.target.value)
        setOrderList([...filtered])   
    }


    const progressChange = async(action,obj) => {
        // debugger
        try{
            obj.progress = action
            const response = await Axios.put(`${URL}/orders/update_one`, obj)
            //set the state with order 
            const temp = orderList
            const index = temp.find( order => order._id === obj._id)
            temp[index] = obj
            setOrderList([...temp])
            setOrders([...temp])
        } catch(err){
            console.log(err)
        }
    }

    const paymentChange = async (action, obj) => {
        try{
            obj.payment = action
            const response = await Axios.put(`${URL}/orders/update_one`, obj)
            //set the state with order 
            const temp = orderList
            const index = temp.find( order => order._id === obj._id)
            temp[index] = obj
            setOrderList([...temp])
            setOrders([...temp])
        } catch(err){
            console.log(err)
        }


    }

    const noteChange = async (newNote, obj) => {
        try{
            obj.notes = newNote
            const response = await Axios.put(`${URL}/orders/update_one`, obj)
            //set the state with order 
            const temp = orderList
            const index = temp.find( order => order._id === obj._id)
            temp[index] = obj
            setOrderList([...temp])
            setOrders([...temp])
            setNewNote('')
        } catch(err){
            console.log(err)
        }

    }



////////////////////////////////////////////////////////////////////////////////////
////////////////////////// PAGE STARTS ////////////////////////////////////////////    

const colors = {
    'pending': 'red',
    'in progress': 'purple',
    'done': 'green',
    'unpaid': 'red',
    'deposit paid': 'purple',
    'paid': 'green',
}

const colorsInActive = {
    'pending': 'redAndWhite',
    'in progress': 'purpleAndWhite',
    'done': 'greenAndWhite',
    'unpaid': 'redAndWhite',
    'deposit paid': 'purpleAndWhite',
    'paid': 'greenAndWhite',
}



    return <div>    

                        <Modal size={'small'} open={deleteOpen} onClose={deleteClose}>
                            <Modal.Header>Delete Order</Modal.Header>
                            <Modal.Content>
                            <p>Are you sure you want to delete this order?</p>
                            </Modal.Content>
                            <Modal.Actions>
                            <Button onClick={()=> deleteClose()} negative>No</Button>
                            <Button
                                onClick={()=>{deleteClose(); deleteOrder(orderId)}}
                                positive
                                icon='checkmark'
                                labelPosition='right'
                                content='Yes'
                            />
                            </Modal.Actions>
                        </Modal>



                <div className='form_container'>
                    <h1>Search Orders</h1>
                    <label>By Progress:</label>
                            <select onChange={progressSelect}>
                                <option value={'all'}>All</option>
                                <option value={'pending'}>Pending</option>
                                <option value={'inProgress'}>In Progress</option>
                                <option value={'done'}>Done</option>

                            </select>



                    <label>By Payment:</label>
                            <select onChange={paymentSelect}>
                                <option value={'all'}>All</option>
                                <option value={'unpaid'}>Unpaid</option>
                                <option value={'depositPaid'}>Deposit Paid</option>
                                <option value={'paid'}>Paid</option>
                            </select>

                    <label>By Order Number:</label>
                    <input onChange={handleOrderNumber}></input>
                    <button onClick={findByOrderNumberClicked}>Go</button>
                </div>

                
                    {
                     orderList.map((obj)=>{
                        //debugger
                        return (
                         
                            <div className='form_container'>
                                <h4>Order Number: {obj._id}</h4>
                                <h2>Client: {obj.usersDetails.name} {obj.usersDetails.surname}</h2>
                                <h4>Email: {obj.usersDetails.email}</h4>
                                <h4>Contact: {obj.usersDetails.tel}</h4>
                                <h4>Address: {obj.usersDetails.address}</h4>
                               
                                
                               
                                    <button style={styles[colors[obj.progress]]}>Progress Status:  {obj.progress}</button>
                                    <button style={styles[colorsInActive[obj.progress]]}
                                            onClick={()=>progressChange('pending',obj) }
                                    >pending</button>
                                    <button style={styles[colorsInActive[obj.progress]]}
                                            onClick={()=> progressChange('in progress', obj)}
                                    >in progress</button>
                                    <button style={styles[colorsInActive[obj.progress]]}
                                            onClick={()=>progressChange('done', obj)}
                                    >done</button>
                                
                                    <br></br>
                                    <button style={styles[colors[obj.payment]]}>Payment Status:  {obj.payment}</button>
                                    <button style={styles[colorsInActive[obj.payment]]}
                                            onClick={()=>paymentChange('unpaid', obj)}
                                    >unpaid</button>
                                    <button style={styles[colorsInActive[obj.payment]]}
                                            onClick={()=>paymentChange('deposit paid', obj)}
                                    >deposit paid</button>
                                    <button style={styles[colorsInActive[obj.payment]]}
                                            onClick={()=>paymentChange('paid', obj)}
                                    >paid</button>
                                <br></br>

                                <p>Notes: {obj.notes}</p>
                                <textarea onChange={(e)=>setNewNote(e.target.value)}>
                                    
                                </textarea>

                                <button onClick={()=>noteChange(newNote, obj)}>Update Notes</button>
                                <br></br>
                                {
                                    
                                    obj.cart.map((ele)=>{
                                        return (
                                            <div>
                                            <h4>Product: {ele.product}</h4>
                                                <div>
                                                    <img className='img' src = {ele.image}/>
                                                </div>
                                            <h5>Unit price: K{ele.price}</h5>
                                            <h3>Qty: {ele.quantity}</h3>
                                            <h3>Product Total: K{ele.quantity*ele.price}</h3>
                                            <br></br>
                                            </div>
                                        )
                                        
                                    })
                                }
                                    <h2>Order Total: K{totalPrice(obj.cart)}</h2> 

                                <br></br>
                                <button onClick={ ()=>{setOrderId(obj._id); deleteShow()}}>Delete Order</button>

                            </div>
                        );
                     })
                        
                    }
                  
     </div>

}
    
    
export default Orders

const styles = {
    red: {
      backgroundColor: "red"
    },
    purple: {
        backgroundColor: "purple"
    },
    green: {
        backgroundColor: "green"
    },
    redAndWhite: {
        backgroundColor: "white",
        color: 'red',
    },
    purpleAndWhite: {
        backgroundColor: "white",
        color: 'purple',
    },
    greenAndWhite: {
        backgroundColor: "white",
        color: 'green',
    },
}