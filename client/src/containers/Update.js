import React , { useState, useEffect } from 'react'
//import Axios from 'axios' 
import UpdateProducts from '../containers/UpdateProducts'

const Update = (props) => {
    // const [data,setData] = useState({})
    const [products, setProducts] = useState([])
    const [category, setCategory] = useState('All')
    const [updatedCategory, setUpdatedCategory] = useState('')
      
    useEffect(()=>{
        setProducts([...props.products])
    },[props.products])
    
   
    const categorySelect = (e) =>{
        if(e.target.value === 'All'){
            return setProducts([...props.products])
        }
        //debugger
        const filtered = props.products.filter( item => item.categoryId === e.target.value)
        setCategory(e.target.value)
        setProducts([...filtered])
        //debugger
    }

    const changeCategory = (e) => {
        setCategory (e.target.value)
        
    }


    return <div>

        <div className='form_container'>
            <h1>Update Category</h1>

            <label>Select Category:</label>
                <select value={category} onChange={changeCategory}>
                <option value={'All'}>All</option>
                    {
                        props.categories.map((ele,i)=>{
                            return  <option key={i} value={ele._id}>{ele.category}</option>
                        })  
                    }
                </select>


            <label>New Category:</label>
            <input onChange={(e)=>setUpdatedCategory(e.target.value)}
                value={updatedCategory}
                   placeholder='enter your changes here'
                   
            />
            <button onClick= {()=>{props.updateCategory(category, updatedCategory);
                                   setUpdatedCategory('');
            }}>Save Changes</button>
            
            <div>
                <h5>{props.message}</h5>
            </div>
        
        </div>

        <div className='form_container'>
            <h1>Update Products</h1>
      
            <label>Select Category:</label>
                <select value={category} onChange={categorySelect}>
                <option value={'All'}>All</option>
                    {
                        props.categories.map((ele,i)=>{
                            return  <option key={i} value={ele._id}>{ele.category}</option>
                        })  
                    }
                </select>
              
        </div>

            {
           products.map ((obj,i)=>{
            return ( 

                <UpdateProducts obj={obj} 
                                key={i}
                                updateProduct={props.updateProduct}
                         />

            );
           })
           
       }

    </div>
}

export default Update



