import React , { useState } from 'react'
import Axios from 'axios' 

const Login = (props) => {
	const [ form , setValues ] = useState({
		email    : '',
		password : ''
	})

	const [ message , setMessage ] = useState('')

	const handleChange = e => {
       setValues({...form,[e.target.name]:e.target.value})
	}

	const handleSubmit = async (e) => {
		e.preventDefault()
		try{
          const response = await Axios.post(`http://localhost:8080/users/login`,{
          	email:form.email,
          	password:form.password
          })
          setMessage(response.data.message)
          if( response.data.ok ){
              setTimeout( ()=> {
				  props.login(response.data.token,response.data.admin)
				  props.history.push('/')
			  },2000)    
          }
          
		}
        catch(error){
        	console.log(error)
        }
	}
	

	return <div>
	
			<form 
				 onSubmit={handleSubmit}
	             onChange={handleChange}
	             className='login_form_container'>
	         <label>Email</label>    
		     <input name="email"/>
		     <label>Password</label>
		     <input type='password' name="password"/>
			 <p><a href='#'>forgot password?</a></p>
		     <button >Login</button>
		     <div className='message'><h4>{message}</h4></div>
			 
	       </form>

		   <div className = 'createAccount_form_container'>
			<p>Not a Member?</p>
		    <button onClick={()=>props.history.push('/register')}>Create Account</button>
		   </div>
		</div>	
}

export default Login










