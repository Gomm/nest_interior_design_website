import React, {useState, useEffect,} from 'react'
// import axios from 'axios'
// import { URL } from '../utils/url_config' 
// import { Button, Modal } from 'semantic-ui-react'


const UserUpdate = props => {
    const [data,setData] = useState({})
 

    const [access, setAccess] = useState (props.obj.admin)
     //debugger
    return <div className='form_container'>
             <h4>{props.obj.name} {props.obj.surname}</h4>


        <h3>User allowed Admin privilages: {access}</h3>

            <h4 style={styles.blue}>{access === 'true' ? "Access Granted" : 'Access Denied'}</h4>

        
        <button style={styles.green}
                onClick={()=> {setData({...data,admin: true}); setAccess('true')}}>Allow access</button>

        <button style={styles.red}
                onClick={()=> {setData({...data,admin: false}); setAccess('false')}}>Deny access</button>


        <label>Name: </label> 
            <input onChange={(e)=>setData({...data,[e.target.name]:e.target.value})}
                   value={data.name} 
                   name='name' 
                   placeholder = {props.obj.name}/> 
            <br></br>

        <label>Surname: </label> 
            <input onChange={(e)=>setData({...data,[e.target.name]:e.target.value})}
                   value={data.surname} 
                   name='surname' 
                   placeholder = {props.obj.surname}/> 
            <br></br>

        <label>Email: </label> 
            <input onChange={(e)=>setData({...data,[e.target.name]:e.target.value})}
                   value={data.email} 
                   name='email' 
                   placeholder = {props.obj.email}/> 
            <br></br>
        <label>Contact: </label>
            <input onChange={(e)=>setData({...data,[e.target.name]:e.target.value})}
                   value={data.tel} 
                   name='tel'
                   placeholder={props.obj.tel}
            /> 
            <br></br>
        
        <label>Address: </label>
        <input onChange={(e)=>setData({...data,[e.target.name]:e.target.value})}
                   value={data.address} 
                   name='address'
                   placeholder= {props.obj.address}/>
        
        
        <button onClick={()=> props.updateUser(data,props.obj)}>Update User</button>

        <button onClick={ ()=>{props.setUserId(props.obj._id); props.deleteShow()}}>Delete User</button>
    </div>
}

export default UserUpdate

const styles = {
    red: {
      backgroundColor: "red"
    },
    green: {
        backgroundColor: "green"
    },
    blue: {
        color: 'rgb(40, 151, 145)'
    }
}