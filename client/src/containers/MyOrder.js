import React, {useState,useEffect} from 'react'
import axios from 'axios'
import { URL } from '../utils/url_config' 
import { Button, Modal } from 'semantic-ui-react'

const MyOrder  = (props) => {
	const cart = JSON.parse(localStorage.getItem('cart'))
	const [finalCart, setFinalCart] = useState ([])
	
    useEffect(()=>{
		fetchCartItems()
	},[])

//========== REMOVE ALERT SECTION (MODAL)  ==================================
	const [productId, setProductId] = useState()
    const [removeOpen, setRemoveOpen] = useState(false)
    const removeShow = () => setRemoveOpen (true)
    const removeClose = () => setRemoveOpen(false)	
	
	
	const fetchCartItems = async () => {
		try{
			const response = await axios.post(`${URL}/products/find_cart`,{cart})
			response.data.products.forEach( item => {
				const index   = cart.findIndex( ele => ele._id == item._id )
				item.quantity = cart[index].quantity
				
			})
			setFinalCart ([...response.data.products])
			
            //call server to get products in cart from local storage
		}catch(err){
			console.log (err)
		}
	}
	
	
	//=================== HANDLE QUANTITY CHANGE ==============================
		
		
		const handleQuantityChange = (_id,action) => {
			const temp   = finalCart, temp2 = cart
			const index  = temp.findIndex(ele => ele._id === _id)
			const index2 = temp2.findIndex(ele => ele._id === _id)

			temp[index].quantity   =  action === 'decrease' ? temp[index].quantity -= 1 : temp[index].quantity += 1
			temp2[index2].quantity =  action === 'decrease' ? temp2[index2].quantity -= 1 : temp2[index2].quantity += 1

			

			setFinalCart([...temp])
			localStorage.setItem('cart',JSON.stringify(temp2))
			//debugger
	    }



//================== HANDLE REMOVE ITEM ======================================

		const handleRemove = (productId) => {
			let temp     = finalCart, temp2 = cart
			const index  = temp.findIndex(ele => ele._id === productId)
			const index2 = temp2.findIndex(ele => ele._id === productId)

			temp.splice([index],1)
			temp2.splice([index2],1)

			setFinalCart([...temp])
			localStorage.setItem('cart',JSON.stringify(temp2))
			//debugger
		}


        const totalPrice = () => {
		
			let total = 0
			finalCart.forEach( ele => total += ele.price*ele.quantity)
			return total
		}


	
		
//=================== HANDLE PLACE ORDER ==================================
	
	const handlePlaceOrder = () => {
		
	
		
		if (totalPrice() === 0){
			alert('You can\'t place an order because your order is empty')
		} else {
			props.history.push('/checkout');
		}
	}	



////////////////////////////////////////////////////////////////////////////////////
////////////////////////// PAGE STARTS ////////////////////////////////////////////


	return <div className='myOrder'>

						<Modal size={'small'} open={removeOpen} onClose={removeClose}>
                            <Modal.Header>Remove Product</Modal.Header>
                            <Modal.Content>
                            <p>Are you sure you want to remove this product?</p>
                            </Modal.Content>
                            <Modal.Actions>
                            <Button onClick={()=> removeClose()} negative>No</Button>
                            <Button
                                onClick={()=>{removeClose(); handleRemove(productId)}}
                                positive
                                icon='checkmark'
                                labelPosition='right'
                                content='Yes'
                            />
                            </Modal.Actions>
                        </Modal>
	 

	          
			  <div className='form_container'>
				<h1>My Order</h1>
				{
					finalCart.map ((obj,i)=>{
						return ( 
							<div>
								<h1>Product: {obj.product}</h1>
								<h3>Unit Price: K{obj.price}</h3>
									<div>
									<img src = {obj.image}/>
									</div>
								<button disabled={obj.quantity < 2} onClick={()=> handleQuantityChange(obj._id,'decrease')}> - </button>
								     <h4>Qty: {obj.quantity}</h4>
								<button disabled={obj.quantity === 20} onClick={()=> handleQuantityChange(obj._id,'increase')}> + </button>
								<h3>Product Total: K{obj.quantity*obj.price}</h3>
								
								<button onClick={ ()=>{setProductId(obj._id); removeShow()}}>Remove item</button>

							</div>
						);
					})
				
				}
				 <h2>Order Total: K{totalPrice()}</h2> 

				<button onClick={handlePlaceOrder}>Place Order</button>
			  </div>

	       </div>

		    
		   
}

export default MyOrder
