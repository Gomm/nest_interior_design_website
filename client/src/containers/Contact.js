import React, {useState, useEffect} from 'react';
import Axios from 'axios'; 
import { URL } from '../utils/url_config' 


const Contact = (props) => {
    const [data, setData] = useState({})

    const [disable, setDisable] = useState(false)

    useEffect(()=>{
		if(disable !== false){
			sendEmail()
		}
    },[disable])


    const handleChange = (e) => {
        setData({...data,[e.target.name] : e.target.value})
    }

    const handleSubmit = (e) => {
        e.preventDefault()
      
         setDisable(true)
   
    }

    const sendEmail = async () => {
        try{
            const response = await Axios.post(`${URL}/emails/send_email`, data)
            // debugger
            props.history.push('/contactSubmitMessage');
        }catch(error){
            console.log(error)
        } 
    }



    
    return <div>
        
    
        <form className='form_container' onSubmit={handleSubmit}>
            <h1>Contact Form</h1>
            <label>Name: </label>
            <input  name='name'
                    required
                    onChange={handleChange}></input>
           
            <label>Surname: </label>
            <input  name='surname'
                    required
                    onChange={handleChange}></input>
          
            <label>Tel: </label>
            <input  name='tel'
                    required
                    onChange={handleChange}></input>
          
            <label>E-mail: </label>
            <input  name='email'
                    required
                    onChange={handleChange}></input>
            <br></br>
            <p>Please provide a brief description of what it is that you are looking for below?</p>
            <label>Message:</label>
            <textarea   name='message'
                        required
                        onChange={handleChange}></textarea>

            <button disable={disable}>Submit</button>
          
            
           
        </form>

    </div>
    

}

export default Contact;