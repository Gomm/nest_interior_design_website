import React from 'react';
//import Axios from 'axios'; 


const Home = () => {

    
    return <div className='homePage'>
        
    
        <h1>Welcome to Nest Interior Design </h1>
        <h4>Minimalistic design.  Maximum Impact</h4>
        <div className ='homeLayerOne'>
            <img className='homeImage' src ='https://images.unsplash.com/photo-1541123603104-512919d6a96c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80' alt='room pic'/>
            <p>Your home should be a reflection of who you are and the things you love</p>
        </div>

        <div className ='homeLayerTwo'>
            <p>Your Office should not just be a place of work! It should be a place where you and your clients feel comfortable.</p>
            <img className='homeImage' src ='https://images.unsplash.com/photo-1489171078254-c3365d6e359f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' alt='office pic'/>
        </div>

        <div className ='homeLayerThree'>
            <img className='homeImage' src ='https://images.unsplash.com/photo-1541123603104-512919d6a96c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80' alt='room pic'/>
            <p>Your home should be a reflection of who you are and the things you love</p>
        </div>



    </div>
    

}

export default Home;