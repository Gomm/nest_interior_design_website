import React, {useState, useEffect,} from 'react'
import axios from 'axios'
import { URL } from '../utils/url_config' 
import { Button, Modal } from 'semantic-ui-react'
import Axios from 'axios'
import UserUpdate from '../containers/UserUpdate.js'


const Users = (props) => {

    useEffect(()=>{
        findUsers()
    },[])

    const [users, setUsers] = useState ([])
    // const [data, setData] = useState ({})


    const [userId, setUserId] = useState ('')
    const [deleteOpen, setDeleteOpen] = useState(false)
    const deleteShow = () => setDeleteOpen (true)
    const deleteClose = () => setDeleteOpen(false)

    // const [access, setAccess] = useState ()
    


    const findUsers = async () => {
        try{
            const response = await axios.get(`${URL}/users/find_all`)
            // debugger
            setUsers([...response.data.users])
         }catch(err){
          console.log (err)
         }

    }


    const deleteUser = async (_id)=> {
        try {
           //debugger
           const res   = await axios.post(`${URL}/users/remove`,{_id})
           const temp  = users
           const index = temp.findIndex(item=>item._id === _id) 
           temp.splice(index,1)
           //debugger
           setUsers([...temp])
           //debugger
        } catch(err) {
           console.log (err)
        }
     }


     const updateUser = async (data,obj) => {
        for (var key in data){
            obj[key] = data [key]
        }
        try {
            const res = await axios.put(`${URL}/users/update_one`, obj)
            const temp = users
            const index = temp.find( user => user._id === obj._id)
            temp[index] = obj
            setUsers([...temp])
            // setData({})
            // setAccess('')
            
            alert ('User updated succesfully')
            
        }catch(err){
            console.log (err)
        }

     }





    return <div>

                        <Modal size={'small'} open={deleteOpen} onClose={deleteClose}>
                            <Modal.Header>Delete User</Modal.Header>
                            <Modal.Content>
                            <p>Make sure you delete any pending orders that this user might have before deleting this user!   Are you sure you want to delete this user?</p>
                            </Modal.Content>
                            <Modal.Actions>
                            <Button onClick={()=> deleteClose()} negative>No</Button>
                            <Button
                                onClick={()=>{deleteClose(); deleteUser(userId)}}
                                positive
                                icon='checkmark'
                                labelPosition='right'
                                content='Yes'
                            />
                            </Modal.Actions>
                        </Modal>


        {
           users.map ((obj,i)=>{
               obj.admin = obj.admin ?  'true' : 'false' 
            return <UserUpdate obj={obj} 
                         key={i}
                         updateUser={updateUser} 
                         deleteUser={deleteUser}
                         setUserId={setUserId}
                         deleteShow={deleteShow}/>;
           })
           
       }


            
    </div>
}

export default Users

const styles = {
    red: {
      backgroundColor: "red"
    },
    green: {
        backgroundColor: "green"
    },
    blue: {
        color: 'rgb(40, 151, 145)'
    }
}