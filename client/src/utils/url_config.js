const URL = window.location.hostname === 'localhost'
? 'http://localhost:8080'
: 'http://167.172.55.247'

export { URL }
//export {} ==> to export multiple variables