import React , { useState , useEffect } from 'react'
import axios from 'axios'
import { URL } from './utils/url_config' 
import { Helmet } from "react-helmet";
//wherever you use axios remember to use {URL} also
import { BrowserRouter as Router , Route , Redirect } from 'react-router-dom'
import Navbar from './components/Navbar.js'
import Header from './components/Header.js'
import Footer from './components/Footer.js'
import Home from './containers/Home.js'
import About from './containers/About.js'
import Portfolio from './containers/Portfolio.js'
import Contact from './containers/Contact.js'
import ContactSubmitMessage from './components/ContactSubmitMessage.js'
import Shop from './containers/Shop.js'
import Login from './containers/Login.js'
import Register from './containers/Register.js'
import Admin from './containers/Admin.js'
import MyOrder from './containers/MyOrder.js'
import Checkout from './containers/Checkout.js'
import PendingOrders from './containers/PendingOrders.js'
import OrderPlacedMessage from './components/OrderPlacedMessage.js'
import AdminNavbar from './components/AdminNavbar.js'
import Create from './containers/Create.js'
import Update from './containers/Update.js'
import Delete from './containers/Delete.js'
import Orders from './containers/Orders.js'
import Users from './containers/Users.js'


//any semantic to be imported

function App() {

  const [isLoggedIn,setIsLoggedIn] = useState(false)
  const [isAdmin,setIsAdmin] = useState(false)
  const [products, setProducts] = useState([])
  const [categories, setCategories] = useState([])
  //const [users, setUsers] = useState ('')

  
  const token = JSON.parse(localStorage.getItem('token'))
 
  
  const verify_token = async () => {
   if( token === null )return setIsLoggedIn(false)
     try{
          const response = await axios.post(`${URL}/users/verify_token`,{token})
          response.data.succ.admin ? setIsAdmin(true) : setIsAdmin(false) 
          return response.data.ok ? setIsLoggedIn(true) : setIsLoggedIn(false)
     }
     catch(error){
        console.log(error)
     }
  }
  
  useEffect( () => {
     verify_token()
     getCategoriesAndProducts ()
     createCart()
  },[])
//============== CREATE CART =============================

const createCart = () => {
   const cart = JSON.parse(localStorage.getItem('cart'))
   if (cart === null){
      localStorage.setItem('cart', JSON.stringify([]))
   }
}

//============== USERS ===================================




//============== LOGIN / LOGOUT ==========================

  const login  = (token,admin) => {
     localStorage.setItem('token',JSON.stringify(token)) 
     //debugger
     admin ? setIsAdmin(true) : setIsAdmin(false)
     setIsLoggedIn(true)
  }
  const logout = () => {
     localStorage.removeItem('token');
     setIsLoggedIn(false)
     setIsAdmin(false)
     
    // needs to return to home page!!!!!!
  }

  // ================= FIND =============================

  const getCategoriesAndProducts = async() => {
     try{
        const response = await axios.get(`${URL}/products/getCategoriesAndProducts`)
        setProducts([...response.data.products])
        setCategories([...response.data.categories])
     }catch(err){
      console.log (err)
     }
  }

  // =============== UPDATE =========================

  const updateProduct = async (data,category,obj) =>{
     if (category !== 'All products') {
        obj.categoryId = category
     }
     for (var key in data){
         obj[key] = data [key]
     }
     try {
         const res = await axios.put(`${URL}/products/update_one`,obj)
         setProducts([...res.data.products])
         alert ('Product updated succesfully')
     }catch(err){
         console.log (err)
     }
  }

  const updateCategory = async (_id, category) => {
     try {
        const res = await axios.put(`${URL}/categories/update_one`,{_id, category})
        //debugger
         setCategories([...res.data.categories])
         alert ('Category updated succesfully')
      }catch(err){
         console.log (err)
      }

   }

  //================= CREATE =====================


  const createCategory = async (category) =>{
     try {
         const res = await axios.post(`${URL}/categories/create`,{category})
         //debugger
         setCategories([...categories, res.data.created])
         //debugger
     } catch(err) {
         console.log (err)
     }
  }

  const createNewProduct = async (product,category)=> {
     //debugger
      try {
         const res = await axios.post(`${URL}/products/create`,{...product,categoryId:category})
         setProducts([...products, res.data.created])
         //debugger
      } catch(err) {

         console.log (err)
      }
   }

//================== DELETE ========================



const deleteCategory = async (deletedCategory) =>{
   //debugger
   try {
       const res   = await axios.post(`${URL}/categories/remove`,{_id:deletedCategory}) 
       debugger
       const temp  = categories
       const index = temp.findIndex(item=>item._id === deletedCategory) 
       temp.splice(index,1)
       // pop up message (`are you sure you want to delete this category?`)
       //debugger
       setCategories([...temp])
       setProducts([...res.data.removeProducts])
   } catch(err) {
       console.log (err)
   }
}

const deleteProduct = async (_id)=> {
    try {
       //debugger
       const res   = await axios.post(`${URL}/products/remove`,{_id})
       const temp  = products
       const index = temp.findIndex(item=>item._id === _id) 
       temp.splice(index,1)
       //debugger
       setProducts([...temp])
       //debugger
    } catch(err) {
       console.log (err)
    }
 }


////////////////////////////////////////////////////////////////////////////////////
////////////////////////// PAGE STARTS //////////////////////////////////////////// 
  
  return (
     <Router>

   <Helmet>
      <title>Nest Interior Design</title>
      <meta charSet="utf-8" />
      <meta name="description" content="Example implementation of Stripe checkout with React.js" />
      <meta name='keywords' content='stripe checkout, example payment implementation'/>
      <meta name='viewport' content='width=device-width, initial-scale=1'/>
   </Helmet>


        <Header/>
            
       
            {
            
            !isAdmin 
            ? <Navbar isLoggedIn={isLoggedIn} logout={logout} />
            : <AdminNavbar isAdmin={isAdmin} logout={logout} />

            }      
               <Route exact path='/create' render={ props =>  !isAdmin
                                             ? <Redirect to={'/'}/>
                                             : <Create createCategory={createCategory} 
                                                                     createNewProduct={createNewProduct}
                                                                     categories={categories} 
                                                                     products={products} 
                                                                     {...props}/>
               }/>
               <Route exact path='/update' render={ props =>  !isAdmin
                                             ? <Redirect to={'/'}/>
                                             : <Update updateProduct={updateProduct}
                                                                     updateCategory={updateCategory} 
                                                                     categories={categories} 
                                                                     products={products} 
                                                                     {...props}/>
               }/>


               <Route exact path='/delete' render={ props => !isAdmin
                                             ? <Redirect to={'/'}/>
                                             : <Delete deleteProduct={deleteProduct}
                                                                     deleteCategory={deleteCategory} 
                                                                     categories={categories} 
                                                                     products={products} 
                                                                     {...props}/>
               }/>



               <Route exact path='/' component={Home}/>
               <Route exact path='/about' component={About}/>
               <Route exact path='/portfolio' component={Portfolio}/>
               <Route exact path='/contact' component={Contact}/>
               <Route exact path='/shop' render={ props => <Shop categories={categories} 
                                                                 products={products}
                                                                 isLoggedIn={isLoggedIn} 
                                                                 isAdmin={isAdmin}
                                                                 {...props}/>
               }/>

              
               <Route exact path='/login'      render={ props => <Login login={login} {...props}/>} />
               <Route exact path='/register'    component={Register}/>
               <Route exact path='/admin' render={ props => {
                                                   return isAdmin
                                             ? <Redirect to={'/'}/>
                                             : <Admin logout={logout} {...props}/>   
               }}/>

               <Route exact path='/orders' render={ props => <Orders logout={logout} {...props}/>} />
               
               {/*                              render={ props => {
               //                                     return !isAdmin 
               //                               ? <Redirect to={'/'}/>
               //                               : <Orders 
               //                                           logout={logout} {...props}/>   
               // }}/> */}

               <Route exact path='/users'  render={ props => { 
                                                return !isAdmin
                                             ? <Redirect to={'/'}/>
                                             : <Users logout={logout}
                                                      isAdmin={isAdmin}
                                             
                                              {...props}/>  
               }}/>
            

               <Route exact path='/myOrder' render={ props => {
                                                   return !isLoggedIn 
                                             ? <Redirect to={'/'}/>
                                             : <MyOrder logout={logout} {...props}/>   
               }}/>

               <Route exact path='/checkout' render={ props => {
                                                   return !isLoggedIn 
                                             ? <Redirect to={'/'}/>
                                             : <Checkout logout={logout} {...props}/>   
               }}/> 

               <Route exact path='/pendingOrders' render={ props => {
                                                   return !isLoggedIn 
                                             ? <Redirect to={'/'}/>
                                             : <PendingOrders logout={logout} {...props}/>   
               }}/> 

               <Route exact path='/contactSubmitMessage' render={ props => {
                                                   return !isLoggedIn 
                                             ? <Redirect to={'/'}/>
                                             : <ContactSubmitMessage logout={logout} {...props}/>   
               }}/> 
               
               <Route exact path='/orderPlacedMessage' render={ props => {
                                                   return !isLoggedIn 
                                             ? <Redirect to={'/'}/>
                                             : <OrderPlacedMessage logout={logout} {...props}/>   
               }}/> 

         <Footer/>
     </Router>
  );
}

export default App;
