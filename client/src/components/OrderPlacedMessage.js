import React from 'react'


const OrderPlacedMessage = (props) =>{
    
    const returnToSite = () => {
        props.history.push('/')
    }

    return <div className = 'form_container'>
            <img src="/images/logo.png" />
            <h3>Thank you for your order</h3>
            <h4>Claire will be in touch to finalize details as soon as possible.</h4>
            <h3>In the meantime you can find this order in your Pending Orders.</h3>
            <h1>Have a nice day!</h1>
            <button onClick={returnToSite}>Return to Site</button>
    </div>
}

export default OrderPlacedMessage