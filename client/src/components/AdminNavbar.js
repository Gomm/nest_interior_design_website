import React from 'react'
import { NavLink } from 'react-router-dom'

const AdminNavbar = (props) => (
    
   <div className='navbar'>
      <NavLink
        exact
        style={styles.default}
        activeStyle={styles.active}
        to={"/create"}
      >
        Create
      </NavLink>

      <NavLink
        exact
        style={styles.default}
        activeStyle={styles.active}
        to={"/update"}
      >
        Update
      </NavLink>

      <NavLink
        exact
        style={styles.default}
        activeStyle={styles.active}
        to={"/delete"}
      >
        Delete
      </NavLink>

      <NavLink
        exact
        style={styles.default}
        activeStyle={styles.active}
        to={"/shop"}
      >
        Shop
      </NavLink>

      <NavLink
        exact
        style={styles.default}
        activeStyle={styles.active}
        to={"/orders"}
      >
        Orders
      </NavLink>

      <NavLink
        exact
        style={styles.default}
        activeStyle={styles.active}
        to={"/users"}
      >
        Users
      </NavLink>

      

      <button onClick={()=>props.logout()}>logout</button>

    </div>
)
   
export default AdminNavbar

const styles = {
  active: {
    color: "white",
    fontWeight: "bold",
    textDecoration: "double underline 	#DCDCDC",
    textTransform: "uppercase"
  },
  default: {
    textDecoration: "none",
    color: "white",
    fontWeight: "bold",
    letterSpacing: "2px",
    fontSize: "20px"
  }
};
