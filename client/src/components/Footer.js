import React, {useState} from 'react';
//import Axios from 'axios' 

const Footer = () => {

    return <div className='footer'>

        <div className='footer_top'>
            <img src="/images/signature.png" />
        </div>
        <div className='footer_bottom'>
            <p> &copy; Copyright 2020 || dotgommdesign.com</p>
        </div>
            
    </div>
}

export default Footer 