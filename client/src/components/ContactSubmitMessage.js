import React from 'react'


const contactSubmitMessage = (props) =>{
    
    const returnToSite = () => {
        props.history.push('/')
    }

    return <div className = 'form_container'>
         <img src="/images/logo.png" />
            <h3>Thank you for your submission</h3>
            <h4>Claire will respond to you shortly</h4>
            <h1>Have a nice day!</h1>
            <button onClick={returnToSite}>Return to Site</button>
    </div>
}

export default contactSubmitMessage