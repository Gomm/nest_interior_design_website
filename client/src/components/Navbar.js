import React from 'react'
import { NavLink } from 'react-router-dom'

const Navbar = (props) => (
   <div className='navbar'>
      <NavLink
        exact
        style={styles.default}
        activeStyle={styles.active}
        to={"/"}
      >
        Home
      </NavLink>

      {
      !props.isLoggedIn ?
        <NavLink
          exact
          style={styles.default}
          activeStyle={styles.active}
          to={"/about"}
        >
          About
        </NavLink>
        : null
      }


      {
      !props.isLoggedIn ?
        <NavLink
          exact
          style={styles.default}
          activeStyle={styles.active}
          to={"/portfolio"}
        >
          Portfolio
        </NavLink>
        : null
      }

      <NavLink
        exact
        style={styles.default}
        activeStyle={styles.active}
        to={"/contact"}
      >
        Contact
      </NavLink>

      
      <NavLink
        exact
        style={styles.default}
        activeStyle={styles.active}
        to={"/shop"}
      >
        Shop
      </NavLink>

      {
      !props.isLoggedIn ?
      <NavLink
        exact
        style={styles.default}
        activeStyle={styles.active}
        to={"/login"}
      >
        Login
      </NavLink>
      : null
      }

      {
        props.isLoggedIn && !props.isAdmin
        ?       <NavLink
        exact
        style={styles.default}
        activeStyle={styles.active}
        to={"/myOrder"}
      >
        Cart
      </NavLink>
      : null
      }

      {
        props.isLoggedIn && !props.isAdmin
        ?       <NavLink
        exact
        style={styles.default}
        activeStyle={styles.active}
        to={"/pendingOrders"}
      >
        Pending Orders
      </NavLink>

      : null
      }

      {
        props.isLoggedIn && !props.isAdmin
        ?   <button onClick={()=>props.logout()}>logout</button>
        : null
      }


      {
        props.isAdmin
        ?       <NavLink
        exact
        style={styles.default}
        activeStyle={styles.active}
        to={"/admin"}
      >
        Admin
      </NavLink>
      : null
      }


      
   </div>
)
   
export default Navbar

const styles = {
  active: {
    color: "white",
    fontWeight: "bold",
    textDecoration: "double underline 	#DCDCDC",
    textTransform: "uppercase"
  },
  default: {
    textDecoration: "none",
    color: "white",
    fontWeight: "bold",
    letterSpacing: "2px",
    fontSize: "20px"
  },
};
